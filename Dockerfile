# We need to use testing because stable comes with an older version of libcheck
# which doesn't have ck_assert_ptr_nonnull.
FROM debian:testing

RUN apt-get update && apt-get install -y \
  check \
  gcc \
  git \
  libncurses-dev \
  make \
  pkg-config \
  subunit \
  && rm -rf /var/lib/apt/lists/*
